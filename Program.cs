﻿using System;

namespace unit_creation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string name = null;
            int attack_range;
            
            
            Console.WriteLine("Создадим персанажа");

            Console.WriteLine("Введите имя персанажа");
            name = Console.ReadLine();

            Console.WriteLine("1 Персанаж ближнего боя \n2 Персанаж дальнего боя");
            attack_range = Convert.ToInt32(Console.ReadLine());
            
            if(attack_range == 1)
            {
                Melee_Player player = new Melee_Player();
                player.ShowInfo(name,  attack_range);
                
            }

            if(attack_range == 2)
            {
                Range_Player player = new Range_Player();
                player.ShowInfo(name, attack_range);  
            }

        }
    }
}
