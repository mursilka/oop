﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace unit_creation
{
    class Player
    {
        protected int health ;
        protected string name;
        protected int attack_range;

        
        public virtual void ShowInfo(string name,  int range_attac)
        {
            this.name = name;
            
            this.attack_range = range_attac;

            Console.WriteLine("Name: " + name);
            
        }

       
    }

    class Melee_Player : Player
    {
        Knife knife = new Knife();

       

        public override void ShowInfo(string name,  int range_attac)
        {
            
               
            
            Console.WriteLine("Melee damage dealer");
            base.ShowInfo(name,  range_attac);
            this.health = 200;
            
            Console.WriteLine("Health: " + this.health);
            knife.InfoShowInfoWeapon();
        }
    }

    class Range_Player : Player
    {
        Pistol pistol = new Pistol();

       

        public override void ShowInfo(string name,  int range_attac)
        {
            
            
            

            
            Console.WriteLine("Range damage dealer");
            base.ShowInfo(name,  range_attac);
            this.health = 100;

            Console.WriteLine("Health: " + this.health);
            pistol.InfoShowInfoWeapon();
            

        }
    }
}
