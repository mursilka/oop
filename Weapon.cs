﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace unit_creation
{
    class Weapon
    {
        
        protected int damage;
        protected string name;

        public virtual void InfoShowInfoWeapon( )
        {
            this.name = name;
            this.damage = damage;

            Console.WriteLine($"Weapon - {name}");
            Console.WriteLine($"Damage - {damage}");
        }
    }

    class Pistol : Weapon
    {
        public override void InfoShowInfoWeapon()
        {
            this.name = " Pistol";
            this.damage = 25;
            base.InfoShowInfoWeapon();
        }
    }

    class Knife : Weapon
    {
        public override void InfoShowInfoWeapon()
        {
            this.name = " Knife";
            this.damage = 50;
            base.InfoShowInfoWeapon();
        }
    }
}
